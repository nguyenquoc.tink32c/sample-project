package nq.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nq.web.exception.ResourceNotFoundException;
import nq.web.model.Book;
import nq.web.repository.BookRepository;

@CrossOrigin(origins = "http://localhost")
@RestController
@RequestMapping("/api/")
public class BookController {
	@Autowired
	private BookRepository bookRepository;

	// Get books
	@GetMapping("books")
	public ResponseEntity<List<Book>> getAllBook() {
		try {
			List<Book> books = new ArrayList<Book>();

			bookRepository.findAll().forEach(books::add);
			if (books.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(books, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Get book by id
	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable(value = "id") Long bookId) throws ResourceNotFoundException {
		Book book = this.bookRepository.findById(bookId)
				.orElseThrow(() -> new ResourceNotFoundException("Book is not found. ID :" + bookId));
		return ResponseEntity.ok().body(book);
	}
}
