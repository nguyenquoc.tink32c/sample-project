package nq.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nq.web.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
