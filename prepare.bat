@echo off
REM Remove all container, images
FOR /f "tokens=*" %%i IN ('docker ps -a -q') DO docker stop %%i
FOR /f "tokens=*" %%i IN ('docker ps -a -q') DO docker rm %%i
FOR /f "tokens=*" %%i IN ('docker images -a -q') DO docker rmi -f %%i

REM Delete folder db-data
rmdir db-data /S /Q

REM Build Jar
cd spring-boot-backend
mvn clean install -DskipTests=true

echo "Build Done"

pause