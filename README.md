# Build and run project with Docker


**1. Clone poject**

`git clone https://gitlab.com/nguyenquoc.tink32c/sample-project.git`

**2. Build file jar**

```
cd ..\spring-boot-backend
mvn clean install -DskipTests=true
or
./mvnw package -DskipTests
```


**3. Build docker**

```
cd ..
docker compose up
```

**4. Run web**

`https://localhost`
