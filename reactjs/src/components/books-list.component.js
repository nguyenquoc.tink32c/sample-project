import React, { Component } from "react";
import BookService from "../services/books.service";

export default class BooksList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: []
    };
  }

  componentDidMount() {
    BookService.getAll().then(response=>{
            this.setState({books : response.data})
      });
  }
  render() {
    return (
      <div>
          <h1 className="text-center">Books List - QuocN</h1>
          <table className="table table-striped">
              <thead>
                  <tr>
                      <td>ID</td>
                      <td>Name</td>
                  </tr>
              </thead>
              <tbody>
                  {
                      this.state.books.map(
                          book=>
                          <tr key ={book.id}>
                              <td>{book.id}</td>
                              <td>{book.name}</td>
                          </tr>
                      )
                  }
              </tbody>
          </table>
      </div>
    );
  }
}
