import http from "../http-common";

class BookDataService {
  getAll() {
    return http.get("/books");
  }
}

export default new BookDataService();